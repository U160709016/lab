package mehmet.shapes3d;

import mehmet.shapes.Circle;

public class Cylinder extends Circle {
	
	int height;
	
	public Cylinder() {
		super(5);
		//this.radius = 5;
		System.out.println("Cylinder is being created");
	}
	
	public Cylinder(int r) {
		//super(5);
		this(r, 10);
		//height = 10;
		//this.radius = r;
		System.out.println("Cylinder is being created");
	}	
	
	public Cylinder(int r, int h) {
		super(r);
		height = h;
		System.out.println("Cylinder is being created");
	}
	
	public double area() {
		return 2 * super.area() + 2 * Math.PI * radius * height;
	}
	
	public double volume() {
		return super.area() * height;
	}
	
	public String toString() {
		return "radius = " + radius + " height = " + height;
	}

}
