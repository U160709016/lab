package mehmet.shapes3d;

import mehmet.shapes.Square;

public class Cube extends Square {
	
	public Cube() {
		this(3);
	}
	public Cube(int side) {
		super(side);
	}
	
	public int area() {
		return super.area() * 6;
	}
	
	public int volume() {
		return super.area() * side;
	}
	
	public String toString() {
		return "side of cube = " + side;
	}
}