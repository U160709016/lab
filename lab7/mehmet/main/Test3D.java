package mehmet.main;

import mehmet.shapes3d.Cube;
import mehmet.shapes3d.Cylinder;

public class Test3D {

	public static void main(String[] args) {
		
		Cylinder cylinder = new Cylinder(7, 15);
		Cube cube = new Cube();
		
		System.out.println(cube);
		System.out.println(cylinder);
		System.out.println("volume of cube = " + cube.volume());
		System.out.println("area of cube = " + cube.area());
		System.out.println("volume of cylinder = " + cylinder.volume());
		System.out.println("area of cylinder = " + cylinder.area());
		
	}

}

