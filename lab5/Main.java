
public class Main {
	public static void main(String[] args) {
		Rectangle rectangle = new Rectangle();
		rectangle.sideA=5;
		rectangle.sideB=6;
		System.out.println("Area " + rectangle.area());
		System.out.println("Perimeter " + rectangle.perimeter());
		
		Circle circle = new Circle();
		circle.radius = 10;
		
		System.out.println("Area " + circle.area());
		System.out.println("Perimeter " + circle.perimeter());		


	}
}
