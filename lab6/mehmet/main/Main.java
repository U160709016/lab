package mehmet.main;
import java.util.ArrayList;
import mehmet.shapes.*;

public class Main {

	public static void main(String[] args) {

		ArrayList <Circle> circles = new ArrayList <Circle> ();
		
		circles.add(new Circle(5));
		circles.add(new Circle(10));
		circles.add(new Circle(6));
		
		for (int i=0; i < circles.size(); i++) {
			System.out.println(circles.get(i).area());
		}
	}

}
