package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T> implements Stack<T> {
	
	private ArrayList<T> contents = new ArrayList<T>();
	
	StackItem<T> top;

	@Override
	public void push(T item) {
		contents.add(item);
	}
	

	@Override
	public T pop() {
		return contents.remove(contents.size()-1);
	}
	

	@Override
	public boolean empty() {
		return contents.size() == 0;
		
	}
	
	@Override
	public List<T> toList(){
		ArrayList<T> content = new ArrayList<T>();
		
		StackItem<T> current = top;
		while (current != null) {
			content.add(0,current.getItem());
			current = current.getPrevious();
		}
		return content;
	}

}
