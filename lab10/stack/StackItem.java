package stack;

public class StackItem <T> {
	T item;
	private StackItem<T> previous;
	
	public StackItem(T item , StackItem<T> previous ) {
		this.item = item;
		this.previous = previous;
	}

	public StackItem<T> getPrevious() {
		return previous;
	}
	
	public T getItem() {
		return item;
	}
}


