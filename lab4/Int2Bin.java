public class Int2Bin{

	public static void main(String[] args){
		int number = Integer.parseInt(args[0]);
		System.out.println(int2Bin(number));	
	}


	public static String int2Bin(int number){
		if ((number == 0) || (number == 1))
			return number + "";
		return int2Bin(number/2) + (number % 2);
	}
}