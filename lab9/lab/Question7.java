package lab;

public class Question7 {

	public static void main(String[] args) {
		System.out.print(powerN(3,3));

	}
	
	public static int powerN(int num , int pow) {
		if (pow == 0) {
			return 1;
		}else {
			return num * powerN(num, pow-1);
		}
		
	}

}
