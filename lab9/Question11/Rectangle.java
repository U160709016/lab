package Question11;

import Question10.Shape;
public class Rectangle extends Shape {
	
	protected int length;
	protected int width;
	
	public Rectangle (int length , int width) {
		this.length = length;
		this.width = width;
	}

	protected int area() {
		return length * width;
	}

}